// Fill out your copyright notice in the Description page of Project Settings.


#include "BigBerry.h"
#include "SnakeBase.h"

// Sets default values
ABigBerry::ABigBerry()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABigBerry::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABigBerry::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABigBerry::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			float Distance = Snake->ElementSize;
			float Xs[4] = { Distance, Distance, -Distance, -Distance };
			float Ys[4] = { Distance, -Distance, Distance, -Distance };
			for (int i = 0; i < 4; i++)
			{
				FVector CurrentLocation = this->GetTransform().GetLocation();
				FVector NewLocation(CurrentLocation.X + Xs[i], CurrentLocation.Y + Ys[i], 10);
				FTransform NewTransform(NewLocation);
				GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
			}
			this->Destroy();
		}
	}
}

